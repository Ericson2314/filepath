module System.AbstractFilePath where

import System.AbstractFilePath.Types
    ( AbstractFilePath )

isValid :: AbstractFilePath -> Bool
